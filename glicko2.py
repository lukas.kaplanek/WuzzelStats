# -*- coding: utf-8 -*-
"""
    glicko2
    ~~~~~~~

    The Glicko2 rating system.

    :copyright: (c) 2012 by Heungsub Lee
    :license: BSD, see LICENSE for more details.
"""
import math
import numpy as np

__version__ = '0.0.dev'


#: The actual score for win
WIN = 1.
#: The actual score for draw
DRAW = 0.5
#: The actual score for loss
LOSS = 0.


MU = 1500
PHI = 350
SIGMA = 0.06
TAU = 0.1
EPSILON = 0.000001
#: A constant which is used to standardize the logistic function to
#: `1/(1+exp(-x))` from `1/(1+10^(-r/400))`
Q = math.log(10) / 400


class Rating(object):

    def __init__(self, mu=MU, phi=PHI, sigma=SIGMA):
        self.mu = mu
        self.phi = phi
        self.sigma = sigma

    def __repr__(self):
        c = type(self)
        args = (self.mu, self.phi, self.sigma)
        return 'mu=%.3f, phi=%.3f, sigma=%.3f' % args

    def display_rating(self):
        return round(self.mu - (3 * self.phi), 2)

    def display_rating_ext(self):
        return self

    def serialize(self):
        return {
            'mu': self.mu,
            'sigma': self.sigma,
            'phi': self.phi,
            }




class Glicko2(object):

    def __init__(self, mu=MU, phi=PHI, sigma=SIGMA, tau=TAU, epsilon=EPSILON):
        self.mu = mu
        self.phi = phi
        self.sigma = sigma
        self.tau = tau
        self.epsilon = epsilon

    def __repr__(self):
        return '%s/%s/%s/%s/%s' %(self.mu,self.phi, self.sigma, self.epsilon, self.tau)


    def create_rating(self, mu=None, phi=None, sigma=None):
        if mu is None:
            mu = self.mu
        if phi is None:
            phi = self.phi
        if sigma is None:
            sigma = self.sigma
        return Rating(mu, phi, sigma)

    def scale_down(self, rating, ratio=173.7178):
        mu = (rating.mu - self.mu) / ratio
        phi = rating.phi / ratio
        return self.create_rating(mu, phi, rating.sigma)

    def scale_up(self, rating, ratio=173.7178):
        mu = rating.mu * ratio + self.mu
        phi = rating.phi * ratio
        return self.create_rating(mu, phi, rating.sigma)

    def reduce_impact(self, rating):
        """The original form is `g(RD)`. This function reduces the impact of
        games as a function of an opponent's RD.
        """
        return 1 / math.sqrt(1 + (3 * rating.phi ** 2) / (math.pi ** 2))

    def expect_score(self, rating, other_rating, impact):
        #print 1. / (1 + math.exp(-impact * (rating.mu - other_rating.mu)))
        expected_score = 1. / (1 + math.exp(-impact * (rating.mu - other_rating.mu)))
        #print expected_score
        #if expected_score>=0.5 and expected_score<0.55:
        #    expected_score=0.55
        #elif expected_score < 0.5 and expected_score > 0.45:
        #        expected_score = 0.45
        r_expected_score= self.round_nearest(expected_score,0.05)
        #print r_expected_score
        return expected_score

    def determine_sigma(self, rating, difference, variance):
        """Determines new sigma."""
        phi = rating.phi
        difference_squared = difference ** 2
        # 1. Let a = ln(s^2), and define f(x)
        alpha = np.log(rating.sigma ** 2)

        def f(x):
            """This function is twice the conditional log-posterior density of
            phi, and is the optimality criterion.
            """
            tmp = phi ** 2 + variance + np.exp(x)
            a = np.exp(x) * (difference_squared - tmp) / (2 * tmp ** 2)
            b = (x - alpha) / (self.tau ** 2)
            return a - b
        # 2. Set the initial values of the iterative algorithm.
        a = alpha
        if difference_squared > phi ** 2 + variance:
            b = np.log(difference_squared - phi ** 2 - variance)
        else:
            k = 1
            while f(alpha - k * np.sqrt(self.tau ** 2)) < 0:
                k += 1
            b = alpha - k * np.sqrt(self.tau ** 2)
        # 3. Let fA = f(A) and f(B) = f(B)
        f_a, f_b = f(a), f(b)
        # 4. While |B-A| > e, carry out the following steps.
        # (a) Let C = A + (A - B)fA / (fB-fA), and let fC = f(C).
        # (b) If fCfB < 0, then set A <- B and fA <- fB; otherwise, just set
        #     fA <- fA/2.
        # (c) Set B <- C and fB <- fC.
        # (d) Stop if |B-A| <= e. Repeat the above three steps otherwise.
        while abs(b - a) > self.epsilon:
            c = a + (a - b) * f_a / (f_b - f_a)
            f_c = f(c)
            if f_c * f_b < 0:
                a, f_a = b, f_b
            else:
                f_a /= 2
            b, f_b = c, f_c
        # 5. Once |B-A| <= e, set s' <- e^(A/2)
        return np.exp(1) ** (a / 2)

    def rate(self, rating, series):
        # Step 2. For each player, convert the rating and RD's onto the
        #         Glicko-2 scale.
        rating = self.scale_down(rating)
        # Step 3. Compute the quantity v. This is the estimated variance of the
        #         team's/player's rating based only on game outcomes.
        # Step 4. Compute the quantity difference, the estimated improvement in
        #         rating by comparing the pre-period rating to the performance
        #         rating based only on game outcomes.
        d_square_inv = 0
        variance_inv = 0
        difference = 0
        for actual_score, other_rating in series:
            other_rating = self.scale_down(other_rating)
            impact = self.reduce_impact(other_rating)
            expected_score = self.expect_score(rating, other_rating, impact)
            #print ("%s/%s" % (actual_score,expected_score))
            if actual_score<expected_score and actual_score>0.5:
                expected_score=actual_score
            if actual_score>expected_score and actual_score<0.5:
                expected_score=actual_score
            #print (" -> %s/%s" % (actual_score, expected_score))
            variance_inv += impact ** 2 * expected_score * (1 - expected_score)
            difference += impact * (actual_score - expected_score)
            d_square_inv += (
                expected_score * (1 - expected_score) *
                (Q ** 2) * (impact ** 2))
        difference /= variance_inv
        variance = 1. / variance_inv
        denom = rating.phi ** -2 + d_square_inv
        mu = rating.mu + Q / denom * (difference / variance_inv)
        phi = math.sqrt(1 / denom)
        # Step 5. Determine the new value, Sigma', ot the sigma. This
        #         computation requires iteration.
        sigma = self.determine_sigma(rating, difference, variance)
        # Step 6. Update the rating deviation to the new pre-rating period
        #         value, Phi*.
        phi_star = math.sqrt(phi ** 2 + sigma ** 2)
        # Step 7. Update the rating and RD to the new values, Mu' and Phi'.
        phi = 1 / math.sqrt(1 / phi_star ** 2 + 1 / variance)
        mu = rating.mu + phi ** 2 * (difference / variance)
        # Step 8. Convert ratings and RD's back to original scale.
        return self.scale_up(self.create_rating(mu, phi, sigma))


    def rate_1vs1(self, rating1, rating2, result):
        return self.rate(rating1, [(result, rating2)])

    def quality_1vs1(self, rating1, rating2):
        expected_score1 = self.expect_score(rating1, rating2, self.reduce_impact(rating1))
        expected_score2 = self.expect_score(rating2, rating1, self.reduce_impact(rating2))
        expected_score = (expected_score1 + expected_score2) / 2
        return 2 * (0.5 - abs(0.5 - expected_score))

    def quality_team(self, ratings1, ratings2):
        quality=0
        n=0
        for i in ratings1:
            for o in ratings2:
                n+=1;
                quality += self.quality_1vs1(i,o)
        quality = quality/n

        #rating_1_avg = Rating(mu=sum([x.mu for x in ratings1])/2, phi=sum([x.phi for x in ratings1])/2, sigma=sum([x.sigma for x in ratings1])/2)
        #rating_2_avg = Rating(mu=sum([x.mu for x in ratings2]) / 2, phi=sum([x.phi for x in ratings2])/2, sigma=sum([x.sigma for x in ratings2])/2)
        #expected_score1 = self.expect_score(rating_1_avg, rating_2_avg, self.reduce_impact(rating_1_avg))
        #expected_score2 = self.expect_score(rating_2_avg, rating_1_avg, self.reduce_impact(rating_2_avg))
        #expected_score = (expected_score1 + expected_score2) / 2
        #return 2 * (0.5 - abs(0.5 - expected_score))
        return quality


    def expected_scores(self, ratings1, ratings2):
        rating_1_avg = Rating(mu=sum([x.mu for x in ratings1])/2, phi=sum([x.phi for x in ratings1])/2, sigma=sum([x.sigma for x in ratings1])/2)
        rating_2_avg = Rating(mu=sum([x.mu for x in ratings2]) / 2, phi=sum([x.phi for x in ratings2])/2, sigma=sum([x.sigma for x in ratings2])/2)
        expected_score1 = self.expect_score(rating_1_avg, rating_2_avg, self.reduce_impact(rating_1_avg))
        expected_score = (expected_score1)
        return int(round(self.score_return(expected_score),0))

    def expected_scores_for_pred(self, ratings1, ratings2):
        rating_1_avg = Rating(mu=sum([x.mu for x in ratings1])/2, phi=sum([x.phi for x in ratings1])/2, sigma=sum([x.sigma for x in ratings1])/2)
        rating_2_avg = Rating(mu=sum([x.mu for x in ratings2]) / 2, phi=sum([x.phi for x in ratings2])/2, sigma=sum([x.sigma for x in ratings2])/2)
        expected_score1 = self.expect_score(rating_1_avg, rating_2_avg, self.reduce_impact(rating_1_avg))
        expected_score = (expected_score1)
        #print expected_score-0.5
        return abs(expected_score-0.5)
        #return int(round(self.score_return(expected_score),0))

    def score_var (self,score_t1, score_t2):
        r_score_t1 = 1 - ((10 - (score_t1 - score_t2)) * 0.05)
        r_score_t2 = 1 - ((10 - (score_t2 - score_t1)) * 0.05)
        return r_score_t1,r_score_t2
    def score_return (self,score):
        if(score>0.5and score<0.55):
            r_score= 1
        elif(score<0.5 and score >0.45):
            r_score= -1
        else:
            r_score=((((-self.round_nearest(score,0.05)+1))/0.05)-10)*-1
        return r_score

    def round_nearest(self,x,a):
        return round(x/a)*a
