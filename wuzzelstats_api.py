from trueskill import *
import numpy as np
import datetime as dt
import pprint

from flask import Flask, request
from flask_restful import Resource, Api, reqparse, abort

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('Player1MU')
parser.add_argument('Player1SIGMA')
parser.add_argument('Player2MU')
parser.add_argument('Player2SIGMA')
parser.add_argument('Player3MU')
parser.add_argument('Player3SIGMA')
parser.add_argument('Player4MU')
parser.add_argument('Player4SIGMA')
parser.add_argument('T1Score')
parser.add_argument('T2Score')


class Player(object):
    def __init__(self, userid, name, mu=None, sigma=None):
        self.name = name
        self.userid = userid
        self.rating = 0
        self.rating_history = []
        if mu is not None and sigma is not None:
            self.update_rating(Rating(mu, sigma))
        else:
            self.update_rating(Rating())

    def update_rating(self, rating):
        current = np.datetime64(dt.datetime.now())
        self.rating_history.append((current, rating))
        self.rating = rating


    def print_history(self):
        sorted_list = np.array(sorted(np.array(self.rating_history), key=lambda x: x[0]))
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(sorted_list)

    def print_player(self):
        print('{0} with a rating of {1} with a variation of {2}'.format(self.name, self.rating.mu, self.rating.sigma))


class Match(Resource):
    def post(self):
        args = parser.parse_args()
        #print(args['Player1MU'])
        #print(args['Player1SIGMA'])

        if args['Player1MU'] is not None and args['Player1SIGMA'] is not None:
            p1 = Player(1, "1",int(args['Player1MU']), float(args['Player1SIGMA']))
        else:
            p1 = Player(1, "1")

        if args['Player2MU'] is not None and args['Player2SIGMA'] is not None:
            p2 = Player(2, "2", int(args['Player2MU']), float(args['Player2SIGMA']))
        else:
            p2 = Player(2, "2")

        if args['Player3MU'] is not None and args['Player3SIGMA'] is not None:
            p3 = Player(3, "3", int(args['Player3MU']), float(args['Player3SIGMA']))
        else:
            p3 = Player(3, "3")

        if args['Player4MU'] is not None and args['Player4SIGMA'] is not None:
            p4 = Player(4, "4", int(args['Player4MU']), float(args['Player4SIGMA']))
        else:
            p4 = Player(4, "4")



        t1_score = int(args['T1Score'])
        t2_score = int(args['T2Score'])
        t1 = [p1.rating, p2.rating]  # Team A contains 1P and 2P
        t2 = [p3.rating, p4.rating]  # Team B contains 2P and 3P
        #print('{:.1%} chance to draw'.format(quality([t1, t2])))
        (r1, r2), (r3, r4) = rate([t1, t2], ranks=[t1_score*-1, t2_score*-1])
        #p1.print_player()
        return {'Player1': {'mu': r1.mu, 'sigma': r1.sigma}, 'Player2': {'mu': r2.mu, 'sigma': r2.sigma}, 'Player3': {'mu': r3.mu, 'sigma': r3.sigma}, 'Player4': {'mu': r4.mu, 'sigma': r4.sigma}}

api.add_resource(Match, '/')

#args = parser.parse_args()
#curl http://127.0.0.1:5000/ -d 'Player1MU=14' -d 'Player1SIGMA=7'

def main():
    #app.run()
    parse_command_line()
    app = tornado.web.Application(
        [
            (r"/", MainHandler),
            (r"/a/message/new", MessageNewHandler),
            (r"/a/message/updates", MessageUpdatesHandler),
            ],
        cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        xsrf_cookies=True,
        debug=options.debug,
        )
    app.listen(options.port)
    tornado.ioloop.IOLoop.current().start()

if __name__ == '__main__':

    #app.run(debug=True)
    main()

