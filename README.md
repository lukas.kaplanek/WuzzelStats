this project is for our foosball ranking, we got tired of the same constellations of player so i decided to build a ranking to balance the teams :)

its built in python, because i wanted to try a website built in python xD database is a mysql db


i know its not the best solution, and not the cleanest for sure ^^

Features:
* 2on2 Matches only
* Rating is calculated on the fly (currently for better testing)
* trueskill rating
* decay of rating
* getting the best matchup between 4 players

used Frameworks:
* Glicko2
* flask
* flask-login
* flask-sqlalchemy
* sqlalchemy
* bootstrap
* bootstrap-material-design
* bootstrap-material-datetimepicker
* passlib
* bcrypt

Todos:
* better html xD
* more stats
* sources in used Frameworks
* much much more ^^


pip install sqlalchemy
pip install flask
pip install flask-login
pip install flask-sqlalchemy
pip install passlib
pip install bcrypt

db user nicht mit localhost anlegen

