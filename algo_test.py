from glicko2 import Glicko2
from models import *
from app import stats
import multiprocessing
from joblib import Parallel, delayed
from database import db_session

MU = 1000
PHI = 333
SIGMA = 0.1
TAU = 1
EPSILON = 0.000001


player_data = Player.query.all()
player_stats = Player_Stats.query.all()
player_matches = Match_Player.query.all()
matches = Match.query.order_by(Match.matchtime.asc()).all()


def frange(start, stop, step):
    i = start
    while i < stop:
        yield i
        i += step



def processInput(genv):

    players, r_matches, funfacts, percentage = stats.calc_all_ratings(
        i_matches=matches,
        enviroment=genv, i_player_data=player_data, i_player_stats=player_stats, i_player_matches=player_matches)

    #print "finished env:%s -> %s" % (genv,percentage)

    return [genv,percentage]


def test_algo():

    best_env = Glicko2()
    players, my_matches, funfacts, percentage = stats.calc_all_ratings(i_matches=matches,
                                                                       enviroment=best_env, i_player_data=player_data, i_player_stats=player_stats, i_player_matches=player_matches)

    best_percentage = percentage



    x = 0
    genvs = []
    genvs2 = []
    genvs3 = []
    genvs4 = []
    for mu in frange(1000, 5000 , 100):
        for sigma in frange(0.1, 1, 0.1):
                for tau in frange(0.1, 1, 0.1):
                    #print ("%s/%s/%s") % (tau,sigma,phi)
                    if (sigma>=tau):
                         continue
                    if(x>500 and x<=1000):
                        genvs2.append(Glicko2(mu=mu, phi=mu/3, sigma=sigma, epsilon=EPSILON, tau=tau))
                    if(x>1000 and x<=1500):
                        genvs3.append(Glicko2(mu=mu, phi=mu/3, sigma=sigma, epsilon=EPSILON, tau=tau))
                    if(x>1500):
                        genvs4.append(Glicko2(mu=mu, phi=mu/3, sigma=sigma, epsilon=EPSILON, tau=tau))
                    if(x<=500):
                        genvs.append(Glicko2(mu=mu, phi=mu/3, sigma=sigma, epsilon=EPSILON, tau=tau))
                    x+=1
                    #genvs.append(Glicko2(mu=mu, phi=mu/3, sigma=sigma, epsilon=EPSILON, tau=tau))
    num_cores = multiprocessing.cpu_count()
    print len(genvs)
    results = Parallel(n_jobs=num_cores)(delayed (processInput)(i) for i in genvs)
    all_results = results
    all_results.sort(key=lambda x: x[1], reverse=True)
    print " best  after 1 (%s): %s" % (all_results[0][1],all_results[0][0])
    results2 = Parallel(n_jobs=num_cores)(delayed (processInput)(i) for i in genvs2)
    all_results += results2
    all_results.sort(key=lambda x: x[1], reverse=True)
    print " best  after 2 (%s): %s" % (all_results[0][1],all_results[0][0])
    results3 = Parallel(n_jobs=num_cores)(delayed (processInput)(i) for i in genvs3)
    all_results += results3
    all_results.sort(key=lambda x: x[1], reverse=True)
    print " best  after 3 (%s): %s" % (all_results[0][1],all_results[0][0])
    results4 = Parallel(n_jobs=num_cores)(delayed (processInput)(i) for i in genvs4)
    all_results += results4
    all_results.sort(key=lambda x: x[1], reverse=True)
    print " best  after 4 (%s): %s" % (all_results[0][1],all_results[0][0])
    all_results = results+results2+results3+results4

    #print results
    return all_results[0][1],all_results[0][0]

(best_percentage,best_env) = test_algo()
print " best (%s): %s" % (best_percentage,best_env)
