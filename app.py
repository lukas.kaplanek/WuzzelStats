import random

from glicko2 import Glicko2
from statistics import WuzzelStats

from flask import Flask, render_template, request, redirect, url_for, g, flash, jsonify
from flask_login import LoginManager, login_required, login_user, current_user, logout_user

from datetime import timedelta


stats = WuzzelStats(Glicko2())


from database import db_session
from models import *

app = Flask(__name__)
app.secret_key = '1l2kjnieunrin1iuh'  # Change this!

# flask login
login_manager = LoginManager()
login_manager.init_app(app)


##Login Section
@login_manager.user_loader
def load_user(id):
    return User.query.get(id)


@app.route('/')
def index():
    # players =Player.query.order_by(Player.match_count.desc()).all()
    #db_session.expire_all()
    Match_Player.clean_match_index()
    player_data = Player.query.all()
    player_stats = Player_Stats.query.all()
    player_matches = Match_Player.query.all()
    all_matches = Match.query.order_by(Match.matchtime.asc()).all()

    players, matches ,funfacts, percentage= stats.calc_all_ratings(
        i_matches=all_matches, i_player_data=player_data, i_player_stats=player_stats, i_player_matches=player_matches)
    players.sort(key=lambda x: x.getRatingScore().display_rating(), reverse=True)

    return render_template('list_players.html',
                           players=players,funfacts=funfacts)


@app.route('/api/players')
# @login_required
def api_list_players():
    # players =Player.query.order_by(Player.match_count.desc()).all()
    #db_session.expire_all()
    Match_Player.clean_match_index()
    player_data = Player.query.all()
    player_stats = Player_Stats.query.all()
    player_matches = Match_Player.query.all()
    all_matches = Match.query.order_by(Match.matchtime.asc()).all()
    players, matches, funfacts, percentage = stats.calc_all_ratings(
        i_matches=all_matches, i_player_data=player_data, i_player_stats=player_stats, i_player_matches=player_matches)
    players.sort(key=lambda x: x.getRatingScore().display_rating(), reverse=True)

    return jsonify(players=[e.serialize() for e in players])

@app.route('/api/matches')
# @login_required
def api_list_matches():
    # players =Player.query.order_by(Player.match_count.desc()).all()
    #db_session.expire_all()
    Match_Player.clean_match_index()
    player_data = Player.query.all()
    player_stats = Player_Stats.query.all()
    player_matches = Match_Player.query.all()
    all_matches = Match.query.order_by(Match.matchtime.asc()).all()
    players, matches, funfacts, percentage = stats.calc_all_ratings(
        i_matches=all_matches, i_player_data=player_data, i_player_stats=player_stats, i_player_matches=player_matches)
    matches.sort(key=lambda x: x.matchtime, reverse=True)

    return jsonify(matches=[e.serialize() for e in matches])

@app.route('/api/matches/<int:match_id>')
# @login_required
def api_list_match(match_id):
    (match_item, team1_players, team2_players) = stats.calc_match_ratings(match_id)

    return jsonify(match_item.serialize())

@app.route('/list_players')
# @login_required
def list_players():
    # players =Player.query.order_by(Player.match_count.desc()).all()
    #db_session.expire_all()
    Match_Player.clean_match_index()
    player_data = Player.query.all()
    player_stats = Player_Stats.query.all()
    player_matches = Match_Player.query.all()
    all_matches = Match.query.order_by(Match.matchtime.asc()).all()

    players, all_matches, funfacts, percentage = stats.calc_all_ratings(
        i_matches=matches, i_player_data=player_data, i_player_stats=player_stats, i_player_matches=player_matches)
    players.sort(key=lambda x: x.getRatingScore().display_rating(), reverse=True)

    return render_template('list_players.html',
                           players=players,funfacts=funfacts)


@app.route('/list_players_last_week')
# @login_required
def list_players_last_week():
    # players =Player.query.order_by(Player.match_count.desc()).all()
    #db_session.expire_all()
    current_time = datetime.datetime.now()
    oneweekago = current_time - timedelta(weeks=1)
    players, matches, funfacts, percentage = stats.calc_all_ratings(
        Match.query.filter(Match.matchtime >= oneweekago).order_by(Match.matchtime.asc()), 7)
    players.sort(key=lambda x: x.getRatingScore().display_rating(), reverse=True)

    return render_template('list_players.html',
                           players=players,funfacts=funfacts)


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'GET':
        return render_template('register.html')

    username = request.form['username']
    user = User(username, request.form['password'], request.form['email'])
    registered_user = User.query.filter(User.user_name == username).first()
    if registered_user is not None:
        flash("Username already taken please choose another")
        return render_template('register.html')
    db_session.add(user)
    db_session.commit()
    flash('User successfully registered')
    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    username = request.form['username']
    password = request.form['password']

    registered_user = User.query.filter(User.user_name == username, User.is_auth).first()
    if registered_user is None:
        flash('Username or Password is invalid', 'error')
        return redirect(url_for('login'))
    if registered_user.check_password(password):
        login_user(registered_user)
        flash('Logged in successfully')
    else:
        flash('Username or Password is invalid', 'error')
        return redirect(url_for('login'))

    return redirect(request.args.get('next') or url_for('index'))


@app.route('/new_player', methods=['GET', 'POST'])
@login_required
def new_player():
    if request.method == 'POST':
        player_name = request.form['player_name']
        player = Player(player_name, stats.getEnv().create_rating())
        registered_user = Player.query.filter(Player.player_name == player_name).first()
        if registered_user is not None:
            flash("Playername already taken please choose another")
            return render_template('new_player.html')
        db_session.add(player)
        db_session.commit()
        return redirect(url_for('list_players'))
    return render_template('new_player.html')


@app.route('/pre_match', methods=['GET', 'POST'])
@login_required
def pre_match():
    if request.method == 'POST':
        selected_players = request.form.getlist('player')
        players = []
        Match_Player.clean_match_index()
        player_data = Player.query.all()
        player_stats = Player_Stats.query.all()
        player_matches = Match_Player.query.all()
        matches = Match.query.order_by(Match.matchtime.asc()).all()

        all_players, all_matches, funfacts, percentage = stats.calc_all_ratings(
            i_matches=matches, i_player_data=player_data, i_player_stats=player_stats, i_player_matches=player_matches)
        players_team1 = []
        players_team2 = []

        for p in all_players:
            if (int(selected_players[0]) == p.id or int(selected_players[1]) == p.id or int(
                    selected_players[2]) == p.id or int(selected_players[3]) == p.id):
                players.append(p)

        (match_quality, players_team1, players_team2,expected_result_t1,expected_result_t2) = stats.getBestPlayerCombination(players)

        #random.shuffle(players_team1)
        #random.shuffle(players_team2)
        return render_template('new_match.html',
                               team1=players_team1, team2=players_team2, timestamp=datetime.datetime.now(),
                               quality=match_quality, expected_result_t1=expected_result_t1, expected_result_t2=expected_result_t2)
    return render_template('pre_match.html',
                           players=Player.query.order_by(Player.is_bi.desc(), Player.player_name.asc()).all())


@app.route('/new_match', methods=['GET', 'POST'])
@login_required
def new_match():
    if request.method == 'POST':
        #print request
        match = Match(request.form['result_team1'], request.form['result_team2'], request.form['matchtime'])

        db_session.add(match)
        db_session.commit()

        match_player1 = Match_Player(request.form['team1_player1'], 1, match.id, 1,stats.getEnv().create_rating())
        match_player2 = Match_Player(request.form['team1_player2'], 1, match.id, 2,stats.getEnv().create_rating())
        match_player3 = Match_Player(request.form['team2_player1'], 2, match.id, 1,stats.getEnv().create_rating())
        match_player4 = Match_Player(request.form['team2_player2'], 2, match.id, 2,stats.getEnv().create_rating())

        db_session.add(match_player1)
        db_session.add(match_player2)
        db_session.add(match_player3)
        db_session.add(match_player4)
        db_session.commit()
        return redirect(url_for('list_matches'))
        #return render_template('pre_match.html',
        #                   players=Player.query.order_by(Player.is_bi.desc(), Player.player_name.asc()).all())
    Match_Player.clean_match_index()
    player_data = Player.query.all()
    player_stats = Player_Stats.query.all()
    player_matches = Match_Player.query.all()
    matches = Match.query.order_by(Match.matchtime.asc()).all()
    all_players, all_matches , funfacts, percentage= stats.calc_all_ratings(
        i_matches=matches, i_player_data=player_data, i_player_stats=player_stats, i_player_matches=player_matches)
    all_players.sort(key=lambda x: x.player_name, reverse=False)
    all_players.sort(key=lambda x: (x.is_bi), reverse=True)
    return render_template('new_match.html',
                           team1=all_players, team2=all_players, timestamp=datetime.datetime.now(),
                           quality=-1)


@app.route('/list_matches', methods=['GET', 'POST'])
@login_required
def list_matches():
    Match_Player.clean_match_index()
    player_data = Player.query.all()
    player_stats = Player_Stats.query.all()
    player_matches = Match_Player.query.all()
    all_matches = Match.query.order_by(Match.matchtime.asc()).all()

    players, matches, funfacts, percentage = stats.calc_all_ratings(
        i_matches=all_matches, i_player_data=player_data, i_player_stats=player_stats, i_player_matches=player_matches)
    matches.sort(key=lambda x: x.matchtime, reverse=True)
    return render_template('list_matches.html', matches=matches)


@app.route('/view_rules')
@login_required
def view_rules():
    return render_template('view_rules.html')


@app.route('/player/<int:player_id>', methods=['GET', 'POST'])
@login_required
def player_info(player_id):
    player_item = Player.query.get(player_id)
    if player_item is None:
        return redirect(url_for('list_players'))
    if request.method == 'GET':
        (player_item, matches, funfacts,percentage) = stats.calc_all_ratings(this_player_id=player_id)
        return render_template('view_player.html', player=player_item, matches=matches)

    if ('delete.%d' % player_id) in request.form:
        db_session.delete(player_item)
    db_session.commit()
    return redirect(url_for('list_players'))


@app.route('/match/<int:match_id>', methods=['GET', 'POST'])
@login_required
def match_info(match_id):
    (match_item, team1_players, team2_players) = stats.calc_match_ratings(match_id)
    #match_item = Match.query.get(match_id)
    #team1_players = Match_Player.query.filter(Match_Player.match_id == match_item.id, Match_Player.team_id == 1)
    #team2_players = Match_Player.query.filter(Match_Player.match_id == match_item.id, Match_Player.team_id == 2)
    if match_item is None:
        return redirect(url_for('list_matches'))
    if request.method == 'GET':
        return render_template('view_match.html', match=match_item, team1_players=team1_players,
                               team2_players=team2_players)

    if ('delete.%d' % match_id) in request.form:
        db_session.delete(match_item)
    db_session.commit()
    return redirect(url_for('list_matches'))


@app.route('/admin_list_user')
@login_required
def admin_list_user():
    if current_user.user_level != 10:
        return redirect(url_for('index'));
    return render_template('admin_list_user.html',
                           users=User.query.order_by(User.registered_on.desc()).all())


@app.route('/user/<int:user_id>', methods=['GET', 'POST'])
@login_required
def user_info(user_id):
    user_item = User.query.get(user_id)
    if (user_item.id != current_user.id and current_user.user_level != 10):
        return redirect(url_for('index'))
    if user_item is None:
        return redirect(url_for('admin_list_user'))

    if request.method == 'GET':
        return render_template('view_user.html', user=user_item)

    if current_user.user_level == 10:
        if ('is_auth.%d' % user_id) in request.form:
            user_item.is_auth = True
        else:
            user_item.is_auth = False
        user_item.user_level = request.form['user_level']
    if current_user.id == user_item.id:
        logout_user()
    if ('delete.%d' % user_id) in request.form:
        db_session.delete(user_item)
    db_session.commit()

    return redirect(url_for('admin_list_user'))


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.before_request
def before_request():
    g.user = current_user



    # if __name__ == "__main__":
    #   app.run(port=5002, debug=True)
