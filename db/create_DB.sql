DROP TABLE if exists tbl_user ;
CREATE TABLE tbl_user (
  user_id BIGINT AUTO_INCREMENT NOT NULL,
  user_name VARCHAR(120) unique,
  user_password VARCHAR(120) NULL,
  player_id bigint NULL,
  registered_on datetime not null,
  is_auth boolean not null,
  user_level int not null,
  PRIMARY KEY (user_id));
  
DROP TABLE if exists tbl_player;
CREATE TABLE tbl_player (
  player_id BIGINT AUTO_INCREMENT NOT NULL,
  player_name VARCHAR(120) unique not null,
  is_bi boolean ,
  created_on datetime not null,
  PRIMARY KEY (player_id));  

DROP TABLE  if exists tbl_match;
CREATE TABLE tbl_match (
  match_id bigint auto_increment not null,
  matchtime datetime not null,
  result_team1 bigint not null,
  result_team2 bigint not null,
  created_on datetime not null,
  season_id bigint,
  PRIMARY KEY (match_id));    
  
  DROP TABLE if exists tbl_match_player;
CREATE TABLE tbl_match_player (
  match_player_id bigint auto_increment not null,
  match_id bigint not null,
  player_id BIGINT NOT NULL,
  team_id bigint not null,
  created_on datetime not null,
  PRIMARY KEY (match_player_id));    
  

