CREATE OR REPLACE VIEW v_player_stats AS
    SELECT 
		-1 as stats_type,
        a.player_id,
        SUM(CASE
            WHEN b.team_id = 1 THEN m.result_team1
            when b.team_id = 2 then m.result_team2
            ELSE 0
        END) round_for,
        SUM(CASE
            WHEN b.team_id = 1 THEN m.result_team2
            WHEN b.team_id = 2 THEN m.result_team1
            ELSE 0
        END) round_against,
        COUNT(m.match_id) AS matches,
        SUM(CASE
            WHEN
                (b.team_id = 1
                    AND m.result_team1 > m.result_team2)
                    OR (b.team_id = 2
                    AND m.result_team2 > m.result_team1)
            THEN
                1
            ELSE 0
        END) AS win_matches,
        SUM(CASE
            WHEN
                (b.team_id = 1
                    AND m.result_team1 < m.result_team2)
                    OR (b.team_id = 2
                    AND m.result_team2 < m.result_team1)
            THEN
                1
            ELSE 0
        END) AS loss_matches,
        SUM(CASE
            WHEN m.result_team2 = m.result_team1 THEN 1
            ELSE 0
        END) AS draw_matches
    FROM
        django.tbl_player a
            LEFT OUTER JOIN
        (tbl_match_player b
        LEFT JOIN tbl_match m ON (m.match_id = b.match_id)) ON (a.player_id = b.player_id)
    GROUP BY a.player_id
    union all
    SELECT 
		7 as STATS_TYPE,
        a.player_id,
        SUM(CASE
            WHEN b.team_id = 1 THEN m.result_team1
            when b.team_id = 2 then m.result_team2
            ELSE 0
        END) round_for,
        SUM(CASE
            WHEN b.team_id = 1 THEN m.result_team2
            WHEN b.team_id = 2 THEN m.result_team1
            ELSE 0
        END) round_against,
        COUNT(m.match_id) AS matches,
        SUM(CASE
            WHEN
                (b.team_id = 1
                    AND m.result_team1 > m.result_team2)
                    OR (b.team_id = 2
                    AND m.result_team2 > m.result_team1)
            THEN
                1
            ELSE 0
        END) AS win_matches,
        SUM(CASE
            WHEN
                (b.team_id = 1
                    AND m.result_team1 < m.result_team2)
                    OR (b.team_id = 2
                    AND m.result_team2 < m.result_team1)
            THEN
                1
            ELSE 0
        END) AS loss_matches,
        SUM(CASE
            WHEN m.result_team2 = m.result_team1 THEN 1
            ELSE 0
        END) AS draw_matches
    FROM
        django.tbl_player a
            LEFT OUTER JOIN
        (tbl_match_player b
        LEFT JOIN tbl_match m ON (m.match_id = b.match_id)) ON (a.player_id = b.player_id and m.matchtime>CURRENT_DATE()-7)
    GROUP BY a.player_id;
    
   