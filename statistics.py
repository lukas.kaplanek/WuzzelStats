from __future__ import division
#profile = lambda f: f

import itertools

from glicko2 import Glicko2
from models import Player, Match_Player, Match, Player_Stats
from datetime import datetime
from database import db_session

import math




class WuzzelStats():
    env = Glicko2()

    def __init__(self, env):
        self.env=env

    def getEnv(self):
        return self.env

    #@profile
    def calc_all_ratings(self, i_matches=None,display_type=-1, enviroment = None, this_player_id = None, i_player_data=None, i_player_stats=None, i_player_matches=None, i_session=None):
        #print('calc_all_ratings')

        players = {}

        if(enviroment is None):
            self.env = self.env
        else:
            self.env = enviroment

        if(i_session is None):
            self.session = db_session()
        else:
            self.session= i_session

        if i_player_data is None:
            player_data=self.session.query(Player).all()
        else:
            player_data=i_player_data

        if i_player_stats is None:
            player_stats=self.session.query(Player_Stats).all()
        else:
            player_stats=i_player_stats

        if i_player_matches is None:
            player_matches=self.session.query(Match_Player).all()
        else:
            player_matches=i_player_matches
        if i_matches is None:
            matches = self.session.query(Match).order_by(Match.matchtime.asc())
        else:
            matches = i_matches



        for player in player_data:
            player.initRating(self.env.create_rating())
            if player.id not in players.keys():
                player.player_stats=next((x for x in player_stats if x.player_id==player.id and x.stats_type == display_type),None)
                #player.player_stats=Player_Stats.query.filter(Player_Stats.player_id==player.id, Player_Stats.stats_type==display_type).first()
                players[player.id] = player
        r_matches = []

        algo_right = 0
        algo_sum_matches = -1

        for match in matches:
            match.algo_right =algo_right
            match.algo_sum_matches =algo_sum_matches
            team1_players = []
            team2_players = []
            i=0
            this_matches = Match_Player.find_by_matchid(match.id)
            for x in this_matches:
                if x.team_id ==1:
                    team1_players.append(x)
                elif x.team_id==2:
                    team2_players.append(x)

            #team1_players = Match_Player.query.filter(Match_Player.match_id == match.id,Match_Player.team_id == 1).all()
            #team2_players = Match_Player.query.filter(Match_Player.match_id == match.id,Match_Player.team_id == 2).all()
            self.decay(players[team1_players[0].player.id], match.matchtime,team1_players[0].position )
            self.decay(players[team1_players[1].player.id], match.matchtime, team1_players[1].position)
            self.decay(players[team2_players[0].player.id], match.matchtime, team2_players[0].position)
            self.decay(players[team2_players[1].player.id], match.matchtime, team2_players[1].position)

            player1_id = team1_players[0].player.id
            player2_id = team1_players[1].player.id
            player3_id = team2_players[0].player.id
            player4_id = team2_players[1].player.id

            player1_rating = players[player1_id].getRating(team1_players[0].position)
            player2_rating = players[player2_id].getRating(team1_players[1].position)
            player3_rating = players[player3_id].getRating(team2_players[0].position)
            player4_rating = players[player4_id].getRating(team2_players[1].position)

            t1 = [player1_rating, player2_rating]
            t2 = [player3_rating, player4_rating]
            expected_result = self.env.expected_scores(t1, t2)

            n = 0
            for x in player_matches:
                if x.player_id ==player1_id and x.match_id == match.id:
                    n+=1
                    match_player1=x
                    match_player1.setRatingBefore(player1_rating)
                    if n == 4:
                        break
                if x.player_id ==player1_id and x.match_id == match.id:
                    n+=1
                    match_player2=x
                    match_player2.setRatingBefore(player1_rating)
                    if n == 4:
                        break
                if x.player_id ==player1_id and x.match_id == match.id:
                    n+=1
                    match_player3=x
                    match_player3.setRatingBefore(player1_rating)
                    if n == 4:
                        break
                if x.player_id ==player1_id and x.match_id == match.id:
                    n+=1
                    match_player4=x
                    match_player4.setRatingBefore(player1_rating)
                    if n == 4:
                        break

            if this_player_id is not None and ((team1_players[0].player.id == this_player_id) or (team1_players[1].player.id == this_player_id) or (
                        team2_players[0].player.id == this_player_id) or (team2_players[1].player.id == this_player_id)):
                for x in player_matches:
                    if x.player_id ==this_player_id and x.match_id == match.id:
                        this_match_player = x
                        break
                this_match_player.setRatingBefore(players[this_player_id].getRating(this_match_player.position))
                this_match_player.expected_result_t1, this_match_player.expected_result_t2 = self.getResults(expected_result)


            match.match_quality=self.quality(t1,t2)

            actual_result = match.result_team1-match.result_team2
            #print ('expected %s:%s actual'%(expected_result,actual_result))
            algo_sum_matches +=1
            match.algo_sum_matches += 1
            if((expected_result>0 and actual_result>0) or (expected_result<0 and actual_result<0)):
                algo_right+=1
                match.algo_right+=1
            (expected_result_t1,expected_result_t2) = self.getResults(expected_result)
            match.expected_result_t2=expected_result_t2
            match.expected_result_t1=expected_result_t1


            (Gresult_t1, Gresult_t2) = self.env.score_var(match.result_team1, match.result_team2)

            #print (env.quality_1vs1(players[team1_players[0].player.id].rating,players[team2_players[0].player.id].rating ))
            r1 = self.env.rate(player1_rating,[(Gresult_t1,player3_rating),(Gresult_t1,player4_rating)])
            r2 = self.env.rate(player2_rating,[(Gresult_t1, player3_rating),(Gresult_t1, player4_rating)])
            r3 = self.env.rate(player3_rating,[(Gresult_t2, player1_rating),(Gresult_t2, player2_rating)])
            r4 = self.env.rate(player4_rating,[(Gresult_t2, player1_rating),(Gresult_t2, player2_rating)])
            players[player1_id].setRating(team1_players[0].position, r1)
            players[player2_id].setRating(team1_players[1].position, r2)
            players[player3_id].setRating(team2_players[0].position, r3)
            players[player4_id].setRating(team2_players[1].position, r4)

            players[player1_id].set_last_matchtime(match.matchtime,team1_players[0].position)
            players[player2_id].set_last_matchtime(match.matchtime, team1_players[1].position)
            players[player3_id].set_last_matchtime(match.matchtime, team2_players[0].position)
            players[player4_id].set_last_matchtime(match.matchtime, team2_players[1].position)

            match_player1.setRatingAfter(players[player1_id].getRating(team1_players[0].position))
            match_player2.setRatingAfter(players[player2_id].getRating(team1_players[1].position))
            match_player3.setRatingAfter(players[player3_id].getRating(team2_players[0].position))
            match_player4.setRatingAfter(players[player4_id].getRating(team2_players[1].position))

            if this_player_id is not None and (team1_players[0].player.id == this_player_id or team1_players[1].player.id == this_player_id or \
                            team2_players[0].player.id == this_player_id or team2_players[1].player.id == this_player_id):
                this_match_player.setRatingAfter(players[this_player_id].getRating(this_match_player.position))
                r_matches.append(this_match_player)
            elif this_player_id is None:
                r_matches.append(match)
            #print match.id
            #if(algo_sum_matches>0):
            #    break

        r_players = []
        percentage=round(100*(algo_right/algo_sum_matches),2)

        r_funfact= ('i got %s%%(%s) out of %s games correct '%(percentage,algo_right,algo_sum_matches))
        for p in players.values():
            self.decay(p, datetime.now(),1)
            self.decay(p, datetime.now(),2)
            r_players.append(p)
        #r_matches.sort(key=lambda x: x.matchtime, reverse=True)
        if this_player_id is not None:
            r_player = players[this_player_id]
            r_matches.sort(key=lambda x: x.match.matchtime, reverse=True)
            return r_player, r_matches,[r_funfact],percentage
        else:
            r_matches.sort(key=lambda x: x.matchtime, reverse=True)
            return r_players, r_matches,[r_funfact], percentage

    def calc_match_ratings(self, this_match_id):
        print('calc_one_ratings')
        players = {}
        r_match_p1 = None
        r_match_p2 = None
        r_match_p3 = None
        r_match_p4 = None
        r_match =None
        r_team1_players =[]
        r_team2_players = []

        players_data = Player.query.all()
        matches = Match.query.order_by(Match.matchtime.asc())
        for player in players_data:
            if player.id not in players.keys():
                player.initRating(self.env.create_rating())
                players[player.id] = player

        for match in matches:
            team1_players = Match_Player.query.filter(Match_Player.match_id == match.id,
                                                      Match_Player.team_id == 1).all()
            team2_players = Match_Player.query.filter(Match_Player.match_id == match.id,
                                                      Match_Player.team_id == 2).all()

            self.decay(players[team1_players[0].player.id], match.matchtime,team1_players[0].position )
            self.decay(players[team1_players[1].player.id], match.matchtime, team1_players[1].position)
            self.decay(players[team2_players[0].player.id], match.matchtime, team2_players[0].position)
            self.decay(players[team2_players[1].player.id], match.matchtime, team2_players[1].position)
            player1_id = team1_players[0].player.id
            player2_id = team1_players[1].player.id
            player3_id = team2_players[0].player.id
            player4_id = team2_players[1].player.id

            player1_rating = players[player1_id].getRating(team1_players[0].position)
            player2_rating = players[player2_id].getRating(team1_players[1].position)
            player3_rating = players[player3_id].getRating(team2_players[0].position)
            player4_rating = players[player4_id].getRating(team2_players[1].position)

            t1 = [player1_rating, player2_rating]
            t2 = [player3_rating, player4_rating]

            if (this_match_id == match.id):
                expected_result=self.env.expected_scores(t1,t2)
                r_match_p1 = Match_Player.query.filter(Match_Player.player_id == team1_players[0].player.id,
                                                       Match_Player.match_id == this_match_id).first()
                r_match_p1.player_r = players[team1_players[0].player.id]
                r_match_p1.setRatingBefore(player1_rating)

                r_match_p2 = Match_Player.query.filter(Match_Player.player_id == team1_players[1].player.id,
                                                       Match_Player.match_id == this_match_id).first()
                r_match_p2.player_r = players[team1_players[1].player.id]
                r_match_p2.setRatingBefore(player2_rating)

                r_match_p3 = Match_Player.query.filter(Match_Player.player_id == team2_players[0].player.id,
                                                       Match_Player.match_id == this_match_id).first()
                r_match_p3.player_r = players[team2_players[0].player.id]
                r_match_p3.setRatingBefore(player3_rating)

                r_match_p4 = Match_Player.query.filter(Match_Player.player_id == team2_players[1].player.id,
                                                       Match_Player.match_id == this_match_id).first()
                r_match_p4.player_r = players[team2_players[1].player.id]
                r_match_p4.setRatingBefore(player4_rating)

                match.expected_result_t1, match.expected_result_t2 = self.getResults(expected_result)



            #(r1, r2), (r3, r4) = rate([t1, t2], ranks=[match.result_team1 * -1, match.result_team2 * -1])

            (Gresult_t1, Gresult_t2) = self.env.score_var(match.result_team1, match.result_team2)

            r1 = self.env.rate(player1_rating,
                          [(Gresult_t1, player3_rating),
                           (Gresult_t1, player4_rating)])
            r2 = self.env.rate(player2_rating,
                          [(Gresult_t1, player3_rating),
                           (Gresult_t1, player4_rating)])
            r3 = self.env.rate(player3_rating,
                          [(Gresult_t2, player1_rating),
                           (Gresult_t2, player2_rating)])
            r4 = self.env.rate(player4_rating,
                          [(Gresult_t2, player1_rating),
                           (Gresult_t2, player2_rating)])
            players[player1_id].setRating(team1_players[0].position, r1)
            players[player2_id].setRating(team1_players[1].position, r2)
            players[player3_id].setRating(team2_players[0].position, r3)
            players[player4_id].setRating(team2_players[1].position, r4)

            players[player1_id].set_last_matchtime(match.matchtime,team1_players[0].position)
            players[player2_id].set_last_matchtime(match.matchtime, team1_players[1].position)
            players[player3_id].set_last_matchtime(match.matchtime, team2_players[0].position)
            players[player4_id].set_last_matchtime(match.matchtime, team2_players[1].position)

            if (this_match_id == match.id):
                r_match =match
                r_match_p1.setRatingAfter( r1)
                r_match_p2.setRatingAfter(r2)
                r_match_p3.setRatingAfter(r3)
                r_match_p4.setRatingAfter(r4)

                r_team1_players=[r_match_p1,r_match_p2]
                r_team2_players=[r_match_p3,r_match_p4]


        return r_match, r_team1_players, r_team2_players

    def decay(self, player, matchtime, position):
        if player.get_last_matchtime(position) is not None:
            time_between = (matchtime.date() - player.get_last_matchtime(position).date())
            #print '%s with %s days with mu %s and sigma %s'% (player.rating.phi,abs(time_between.days), player.rating.mu, player.rating.sigma)
            new_phi_1 = math.sqrt(player.getRating(position).phi**2 + abs(time_between.days)*100000*player.getRating(position).sigma**2)
            #new_phi_2 = math.sqrt(player.getRating(2).phi ** 2 + ((player.getRating(2).sigma) ** 2 + (player.getRating(2).mu / 100) ** 2) * abs(time_between.days) ** 2)
            #print '%s with %s days with mu %s and sigma %s'% (new_phi, abs(time_between.days), player.rating.mu,player.rating.sigma)

            player.setRating(position, self.env.create_rating(mu=player.getRating(position).mu,sigma= player.getRating(position).sigma, phi=new_phi_1))

            #player.setRating(2,self.env.create_rating(mu=player.getRating(2).mu, sigma=player.getRating(2).sigma, phi=new_phi_2))

    def quality(self, ratings1, ratings2):
        return self.env.quality_team(ratings1,ratings2)

    def expected_scores(self, ratings1, ratings2):
        return self.env.expected_scores(ratings1,ratings2)

    def getBestPlayerCombination(self, players):
        combin=[]
        combin.append(TeamPositions(1,2))
        combin.append(TeamPositions(2,1))
        players_team1 = []
        players_team2 = []
        match_quality =0
        expected_result=1

        for team in itertools.combinations(players, 2):
            enemy = list(set(players)-set(team))
            teamx = list(set(players)-set(enemy))
            for c in combin:
                for c_e in combin:
                    #print ("%s/%s vs %s/%s" % (c.get1(),c.get2(),c_e.get1(),c_e.get2() ))
                    my_match_quality = self.quality([teamx[0].getRating(c.get1()), teamx[1].getRating(c.get2())],
                                                 [enemy[0].getRating(c_e.get1()), enemy[1].getRating(c_e.get2())])
                    my_expected_result = self.env.expected_scores_for_pred(
                        [teamx[0].getRating(c.get1()), teamx[1].getRating(c.get2())],[enemy[0].getRating(c_e.get1()), enemy[1].getRating(c_e.get2())])

                    if(my_expected_result<expected_result):
                        #print "got new high"
                        #print("%s for %s vs %s" % (my_match_quality, teamx, enemy ))
                        #print("%s / %s vs %s / %s" % (teamx[0].getRating(c.get1()),teamx[1].getRating(c.get2()),enemy[0].getRating(c_e.get1()),enemy[1].getRating(c_e.get2()) ))

                        players_team1 = []
                        players_team2 = []
                        match_quality=my_match_quality
                        if c.get1()==1:
                            players_team1.append(teamx[0])
                            players_team1.append(teamx[1])
                        else:
                            players_team1.append(teamx[1])
                            players_team1.append(teamx[0])

                        if c_e.get1() == 1:
                            players_team2.append(enemy[0])
                            players_team2.append(enemy[1])
                        else:
                            players_team2.append(enemy[1])
                            players_team2.append(enemy[0])
                        expected_result = self.env.expected_scores_for_pred(
                            [players_team1[0].getRating(1), players_team1[1].getRating(2)],
                            [players_team2[0].getRating(1), players_team2[1].getRating(2)])
                        #print expected_result

        expected_result = self.env.expected_scores(
            [players_team1[0].getRating(1), players_team1[1].getRating(2)],
            [players_team2[0].getRating(1), players_team2[1].getRating(2)])
        expected_result_t1,expected_result_t2=self.getResults(expected_result)

        return (match_quality,players_team1, players_team2, expected_result_t1, expected_result_t2)

    def getResults(self, expected_result):
        if (expected_result > 0):
            expected_result_t1 = 10
            expected_result_t2 = 10 - expected_result
        elif(expected_result == 10):
            expected_result_t1 = 10
            expected_result_t2 = 10
        else:
            expected_result_t2 = 10
            expected_result_t1 = 10 - abs(expected_result)
        return expected_result_t1,expected_result_t2


class TeamPositions():
    def __init__(self,team1,team2):
        self.team1=team1
        self.team2=team2

    def get1(self):
        return self.team1
    def get2(self):
        return self.team2
