
from sqlalchemy import *
from sqlalchemy.orm import relationship, column_property, reconstructor
from flask_login import UserMixin
from collections import defaultdict
from database import Base
from passlib.hash import bcrypt

import datetime

from glicko2 import Rating


class Match_Player(Base):
    __tablename__ = "tbl_match_player"
    match_index=defaultdict(list)

    id = Column('match_player_id', Integer, primary_key=True, unique=True)
    match_id = Column(Integer, ForeignKey('tbl_match.match_id'))
    player_id = Column(Integer, ForeignKey('tbl_player.player_id'))
    team_id = Column(Integer)
    position = Column(Integer)
    created_on = Column('created_on', DateTime)
    winner_loser = '?'

    last_matchtime = None
    expected_result = 0.5

    player = relationship("Player", foreign_keys=[player_id], backref="Player.matches")
    match = relationship("Match", foreign_keys=[match_id], backref="Match.players")

    def __init__(self, player_id, team_id, match_id, position, rating):
        #print "Match_Player init"
        self.player_id = player_id
        self.team_id = team_id
        self.position = position
        self.match_id = match_id
        self.created_on = datetime.datetime.now()
        self.rating_after_1 = rating
        self.rating_before_1 = rating
        self.rating_after_2 = rating
        self.rating_before_2 = rating
        self.expected_result = 0.5
        Match_Player.match_index[self.match_id].append(self)



    @reconstructor
    def init_on_load(self):
        #print "Match_Player init on load"
        Match_Player.match_index[self.match_id].append(self)
        if self.match.result_team1 > self.match.result_team2 and self.team_id==1:
            self.winner_loser='W'
        elif self.match.result_team2 > self.match.result_team1 and self.team_id==2:
            self.winner_loser='W'
        elif self.match.result_team2 == self.match.result_team1:
            self.winner_loser='D'
        else:
            self.winner_loser='L'
    @classmethod
    def find_by_matchid(cls,matchid):
        return Match_Player.match_index[matchid]
    @classmethod
    def clean_match_index(cls):
        Match_Player.match_index=defaultdict(list)

    def init_rating(self, rating):
        self.rating_after_1 = rating
        self.rating_before_1 = rating
        self.rating_after_2 = rating
        self.rating_before_2 = rating

    def setRatingBefore(self,new_rating):
        if self.position ==1:
            self.rating_before_1 = new_rating
        elif self.position==2:
            self.rating_before_2 = new_rating
        else:
            print "this position is not implemeted"
        #print self.rating[position]

    def getRatingBefore(self):
        if self.position ==1:
            return self.rating_before_1
        elif self.position==2:
            return self.rating_before_2
        else:
            print "this position is not implemeted"
            return None

    def setRatingAfter(self,new_rating):
        if self.position ==1:
            self.rating_after_1 = new_rating
        elif self.position==2:
            self.rating_after_2 = new_rating
        else:
            print "this position is not implemeted"
        #print self.rating[position]

    def getRatingAfter(self):
        if self.position ==1:
            return self.rating_after_1
        elif self.position==2:
            return self.rating_after_2
        else:
            print "this position is not implemeted"
            return None
    def serialize(self):
        return {
            'player': self.player.serialize(),
            'team_id': self.team_id,
            'created_on': self.created_on,
            'position': self.position,
            'rating_before': self.getRatingBefore().serialize(),
            'rating_after': self.getRatingAfter().serialize(),
            #'rating_after_1': self.rating_after_1.serialize(),
            #'rating_after_2': self.rating_after_2.serialize(),
            #'rating_before_2': self.rating_before_2.serialize(),
            'match_id': self.match_id,
            'expected_result': self.expected_result,
            'winner_loser': self.winner_loser,

        }


class Player(Base):
   # print "PLAYER"
    __tablename__ = "tbl_player"

    #rating_history = []
    rating_1 =None
    rating_2 = None
    last_matchtime_1=None
    last_matchtime_2=None


    is_bi = Column(Boolean)

    id = Column('player_id', Integer, primary_key=True, unique=True)
    player_name = Column(String, unique=True)
    created_on = Column('created_on', DateTime)

    matches = relationship("Match_Player")
    player_stats = None

    match_count = column_property(
        select([func.count(Match_Player.match_id)]). \
            where(Match_Player.player_id == id). \
            correlate_except(Match_Player)
    )

    def serialize(self):
        return {
            'player_id': self.id,
            'player_name': self.player_name,
            'created_on': self.created_on,
            'is_bi': self.is_bi,
            'rating_1': self.rating_1.serialize(),
            'rating_2': self.rating_2.serialize(),
        }

    def set_last_matchtime(self,dt, position):
        if position ==1:
            self.last_matchtime_1 = dt
        elif position==2:
            self.last_matchtime_2 = dt
        else:
            print "this position is not implemeted"



    def get_last_matchtime(self, position):
        if position ==1:
            return self.last_matchtime_1
        elif position==2:
            return self.last_matchtime_2
        else:
            print "this position is not implemeted"
            return None
        #return self.last_matchtime

    def __init__(self, player_name, rating):

        self.player_name = player_name
        self.created_on = datetime.datetime.now()
        self.rating_1= rating
        self.rating_2=rating
        #self.rating_history.append((datetime.datetime.now(), self.rating))


    def initRating(self,rating):
        self.rating_1= rating
        self.rating_2=rating
        self.last_matchtime = None

    def setRating(self,position,new_rating):
        if position ==1:
            self.rating_1 = new_rating
        elif position==2:
            self.rating_2 = new_rating
        else:
            print "this position is not implemeted"
        #print self.rating[position]

    def getRating(self,position):
        if position ==1:
            return self.rating_1
        elif position==2:
            return self.rating_2
        else:
            print "this position is not implemeted"
            return None

    def getRatingScore(self):
        new_mu=(self.getRating(1).mu+self.getRating(2).mu)/2
        new_sigma=(self.getRating(1).sigma+self.getRating(2).sigma)/2
        new_phi=(self.getRating(1).phi+self.getRating(2).phi)/2
        return Rating(mu=new_mu,sigma= new_sigma, phi=new_phi)


    def __repr__(self):
        return '<Player %s>' % (self.id)



class User(Base, UserMixin):
    __tablename__ = "tbl_user"

    id = Column('user_id', Integer, primary_key=True, unique=True)
    user_name = Column(String, unique=True, index=True)
    user_password = Column(String)
    registered_on = Column('registered_on', DateTime)
    is_auth = Column(Boolean)
    user_level = Column(Integer)
    player_id = Column(Integer, ForeignKey('tbl_player.player_id'))


    player = relationship("Player", foreign_keys=[player_id])

    def __init__(self, username, password, email):
        self.user_name = username
        self.user_password = self.hash_pw(password)
        self.email = email
        self.registered_on = datetime.datetime.now()
        self.is_auth = False
        self.user_level = 0
        self.player_id = None

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def check_password(self, password):
        return bcrypt.verify(password, self.user_password)

    def hash_pw(self, password):
        return bcrypt.encrypt(password)

    def __repr__(self):
        return '<User %r>' % (self.username)


class Match(Base):
    __tablename__ = "tbl_match"

    id = Column('match_id', Integer, primary_key=True, unique=True)
    matchtime = Column(DateTime)
    result_team1 = Column(Integer)
    result_team2 = Column(Integer)
    created_on = Column('created_on', DateTime)

    match_quality = 0
    expected_result_t1 = 0
    expected_result_t2 = 0

    players = relationship("Match_Player", cascade="all, delete, delete-orphan")

    def __init__(self, result_team1, result_team2, matchtime):
        self.result_team1 = result_team1
        self.result_team2 = result_team2
        self.matchtime = datetime.datetime.strptime(matchtime, '%d.%m.%Y %H:%M')
        self.created_on = datetime.datetime.now()


    def serialize(self):
        return {
            'match_id': self.id,
            'matchtime': self.matchtime,
            'created_on': self.created_on,
            'result_team1': self.result_team1,
            'result_team2': self.result_team2,
            'match_quality': self.match_quality,
            'expected_result_t1': self.expected_result_t1,
            'expected_result_t2': self.expected_result_t2,
            'players':[e.serialize() for e in self.players],

        }
    def __repr__(self):
        return 'Matchid: %s  Matchtime: %s' % (
            self.id, self.matchtime.strftime('%Y-%m-%d %H:%M'))


class Player_Stats(Base):
    __tablename__ = "v_player_stats"

    stats_type = Column(Integer, primary_key=True)
    player_id = Column('player_id', Integer, ForeignKey('tbl_player.player_id'), primary_key=True)
    round_for = Column(Integer)
    round_against = Column(Integer)
    matches = Column(Integer)
    win_matches = Column(Integer)
    loss_matches = Column(Integer)
    draw_matches = Column(Integer)

    #player = relationship("Player", foreign_keys=[player_id], backref="Player.player_stats")
